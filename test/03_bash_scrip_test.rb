require 'minitest/autorun'

class BashScriptTest < MiniTest::Unit::TestCase
  EXECUTABLE = File.expand_path(File.dirname(__FILE__) + '/../primes_matrix')

  def test__calling_the_script_without_arguments_generate_a_matrix_of_size_10
    `#{EXECUTABLE}`.must_match /Multiplication table for the first 10 prime numbers./
  end

  def test__the_first_argument_is_the_matrix_size
    `#{EXECUTABLE} 3`.must_match /Multiplication table for the first 3 prime numbers./
  end

end
