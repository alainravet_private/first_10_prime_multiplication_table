require_relative '../lib/prime_matrix'
require 'minitest/autorun'

class PrimeMatrixTest < MiniTest::Unit::TestCase

  def setup
    @matrix = PrimeMatrix.new(nof_primes=3)
  end
  def test__cells

    @matrix.cells.must_equal [
                                [nil,  2,  3,  5],  # row 0 == the *columns* header
                                [  2,  4,  6, 10],  # row 1
                                [  3,  6,  9, 15],  # row 2
                                [  5, 10, 15, 25],  # row 3
                            ]   # ^
  end                           # col 0 = the *rows* header



  EXPECTED_OUTPUT = <<-TEXT.chomp
Multiplication table for the first 3 prime numbers.
---------------------------------------------------
     2  3  5
  2  4  6 10
  3  6  9 15
  5 10 15 25
  TEXT

  def test__to_text__is_a_formatted_representation_of_the_matrix
    @matrix.to_text.must_equal EXPECTED_OUTPUT
  end
end
