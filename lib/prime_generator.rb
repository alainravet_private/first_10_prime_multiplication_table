# Standard formulas for #is_prime? and #next_prime are widely available.
# My adaptation below is tested by 01_prime_generator_test.rb
# source: http://stackoverflow.com/questions/11673968/generating-first-n-prime-numbers

# Usage :
#   first_ten_primes = PrimeGenerator.take(10)
#   => [2, 3, 5, 7, 11, 13, 17, 19, 23, 29]

PrimeGenerator = Enumerator.new do |y|
    first_prime = 2
    candidate   = first_prime
    loop do
      y << candidate
      candidate = candidate.next_prime
    end
end


# The generator above requires Integer#next_prime
# We cl
module PrimeNumberFinding
  # Usage :
  #   if 2.is_prime? ...
  #
  def is_prime?                         # Simple but fast enough
    n = self                            # to generate a few thousands
    return false if n <= 1              # prime numbers.
    2.upto(Math.sqrt(n).to_i) do |x|    # (see benchmark in 01_prime_generator_test.rb)
      is_multiple_of = self % x == 0    #
      return false if is_multiple_of    #
    end                                 #
  end

  # Usage :
  #   unless 3 == 2.next_prime ..
  #
  def next_prime                        # Complex looking but actually really simple.
    candidate = self + 1                # (could be optimized, but ..)
    until candidate.is_prime?           #
      candidate = candidate + 1         #
    end                                 #
    candidate                           #
  end                                   #
end
Integer.send :include, PrimeNumberFinding

