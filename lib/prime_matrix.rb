require_relative 'prime_generator'

class PrimeMatrix
  attr_reader :cells, :size, :primes

  def initialize(n_of_primes)
    @size   = n_of_primes
    @primes = PrimeGenerator.take(size)
    @cells  = []
    build_the_matrix
  end

  def to_text
    longest_number  = (@primes.last**2).to_s
    cell_text_width = longest_number.length + 1

    result = ["Multiplication table for the first #{@size} prime numbers."]
    result << '-'*result.last.length
    cells.each do |row_cells|
      line = row_cells.collect do |cell|
        cell.to_s.rjust(cell_text_width)
      end.join
      result << line
    end
    result.join("\n")
  end

private

  def build_the_matrix
    columns_header = [nil] + primes       # row 0 = the column header (== factor 2)
    cells << columns_header
    primes.each do |row_number|
      row_header = row_number
      row_cells = [row_header]            # column 0 the row header  (== factor 1)
      primes.each do |col_number|
        row_cells << col_number * row_number
      end
      cells << row_cells
    end
  end


end
